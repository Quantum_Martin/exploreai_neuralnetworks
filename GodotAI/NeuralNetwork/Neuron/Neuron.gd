tool
extends Sprite

var synapse_scene = preload("res://NeuralNetwork/Synapse/Synapse.tscn")

export(Color) var normal_color
export(Color) var hover_color



var is_hovered:bool = false
var is_selected:bool = false


func create_synapses(var to_layer:Node2D, var syn_count, var biases, var nn):
	if(to_layer!=null):
		var syn_diff = get_child_count()-syn_count
	
		while(syn_diff < 0):
			var syn_node = synapse_scene.instance()
			syn_node.init(nn)
			add_child(syn_node)
			syn_diff+=1
		while(syn_diff > 0):
			get_child(syn_count+syn_diff-1).queue_free()
			syn_diff-=1
		
		for i in range(syn_count):
			var line = get_child(i)
			line.clear_points()
			line.add_point(Vector2.ZERO)
			line.add_point( (to_layer.get_child(i).global_position - global_position))
			line.set_slider_position(global_position + (to_layer.get_child(i).global_position - global_position)/2 - Vector2(40,10))
	else:
		for c in get_children():
			c.queue_free()
	if(biases):
		var syn_node = synapse_scene.instance()
		syn_node.init(nn)
		add_child(syn_node)
		syn_node.set_slider_position(global_position - Vector2(40,20))

func _process(delta):
	if(!is_selected and (get_global_mouse_position().distance_to(global_position))<40 and !is_hovered):
		is_hovered = true
		material.set_shader_param("color", hover_color)
	elif(!is_selected and (get_global_mouse_position().distance_to(global_position))>=40 and is_hovered):
		is_hovered = false
		material.set_shader_param("color", normal_color)
		
func _input(event):
	if(is_hovered and !is_selected):
		if(event is InputEventMouseButton and event.button_index == BUTTON_LEFT):
			for neuron in get_tree().get_nodes_in_group("Neurons"):
				neuron.hide_neuron_GUI()
			show_neuron_GUI()

func show_neuron_GUI():
	is_selected = true
	for synapse in get_children():
		synapse.show_slider(true)
	
func hide_neuron_GUI():
	if(get_global_mouse_position().distance_to(global_position)>=40):
		is_selected = false
		for synapse in get_children():
			synapse.show_slider(false)

func get_weights():
	var weights = []
	for i in range(get_child_count()):
		weights.append(get_child(i).weight)
	return weights
