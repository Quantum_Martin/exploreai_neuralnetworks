tool
extends Node2D

var neuron_scene = preload("res://NeuralNetwork/Neuron/Neuron.tscn")

var neuron_count = 0

var neuron_distance = 100

var layer_num = 0
func update_neurons(new_count):
	var neuron_diff = new_count-neuron_count
	var ccount = 0
	while(neuron_diff < 0):
		get_child(new_count+ccount).queue_free()
		ccount+=1
		neuron_diff+=1
	while(neuron_diff > 0):
		neuron_diff-=1
		var neuron_node = neuron_scene.instance()
		add_child(neuron_node)
	
	for i in range(get_child_count()):
		var neuron = get_child(i)
		neuron.position.y = neuron_distance*((new_count/2 +.5*(new_count%2)) - i-.5)
		
	
	neuron_count = new_count
	

func update_synapses(var to_layer, var count, nn):
	for neuron in get_children():
		neuron.create_synapses(to_layer,count, false if layer_num==0 else true, nn)

func get_weights():
	var weights = []
	for i in range(get_child_count()):
		weights += get_child(i).get_weights()
	return weights
