extends Line2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
#func _ready():
#	get_child(0).set_as_toplevel(true)

var weight = .5;
var nn
func init(nn):
	self.nn = nn


func set_slider_position(var pos):
	get_child(0).set_as_toplevel(true)
	$HSlider.rect_position=pos

func show_slider(var b):
	get_child(0).visible = b

func set_weight(w):
	weight = w
	$HSlider.value = weight

func _on_HSlider_value_changed(value):
	weight = value
	width = 1.5 + weight*7.5
	nn.update_weights()
