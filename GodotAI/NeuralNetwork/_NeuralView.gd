extends ColorRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
#	get_parent().find_node("NN_" + name).connect("weight_changed", self, "update_weights")
	$NN_Perceptron2.connect("weight_changed",self,"update_weights")

func update_weights(weights:Array, sizes:Array):
	
	material.set_shader_param("w11", weights[0][0])
	material.set_shader_param("w12", weights[0][1])
	material.set_shader_param("w13", weights[0][2])
	material.set_shader_param("w14", weights[0][3])
	material.set_shader_param("w15", weights[0][4])
	material.set_shader_param("w16", weights[0][5])
	
	material.set_shader_param("w21", weights[1][0])
	material.set_shader_param("w22", weights[1][1])
	material.set_shader_param("b11", weights[1][2])
	
	material.set_shader_param("w23", weights[1][3])
	material.set_shader_param("w24", weights[1][4])
	material.set_shader_param("b12", weights[1][5])
	
	material.set_shader_param("w25", weights[1][6])
	material.set_shader_param("w26", weights[1][7])
	material.set_shader_param("b13", weights[1][8])
	
	material.set_shader_param("b21", weights[2][0])
	material.set_shader_param("b22", weights[2][1])
	
#
#			var h1 = sigmoid(weights[0][0]*ppos.x + weights[0][3]*ppos.y + weights[1][2])
#		var h2 = sigmoid(weights[0][1]*ppos.x + weights[0][4]*ppos.y + weights[1][5])
#		var h3 = sigmoid(weights[0][2]*ppos.x + weights[0][5]*ppos.y + weights[1][8])
#
#
#		var red_out = sigmoid(h1*weights[1][0] + h2*weights[1][3] + h3*weights[1][6] + weights[2][0])
#		var blue_out = sigmoid(h1*weights[1][1] + h2*weights[1][4] + h3*weights[1][7] + weights[2][1])
	
#	var layer_count = weights.size()
#	material.set_shader_param("layer_count", layer_count)
#
#	#var sizes = []
#	var neuron_max = 0
#	for i in range(layer_count):
#		var layer_size = weights[i].size()
#		if layer_size > neuron_max:
#			neuron_max = layer_size
#	#print(neuron_max)
#	var simg = Image.new()
#	simg.create_from_data(layer_count,1,false, Image.FORMAT_R8, sizes)
#	var stexture = ImageTexture.new()
#	stexture.create_from_image(simg,0)
#	material.set_shader_param("sizes", stexture)
#
##	print(weights)
#
#	var byte_array:PoolByteArray
#	for i in range(layer_count):
#		var arr = []
#		for j in range(neuron_max):
#			if(j<weights[i].size()):
#				arr.append(weights[i][j]*255)
#			else:
#				arr.append(0)
#		byte_array.append_array(PoolByteArray(arr))
#
#	var wimg = Image.new()
#	wimg.create_from_data(neuron_max,layer_count,false, Image.FORMAT_RF, weights)
#	var wtexture = ImageTexture.new()
#	wtexture.create_from_image(wimg,0)
#	material.set_shader_param("weights", wtexture)
	
	




func _on_CheckButton_toggled(button_pressed):
	material.set_shader_param("apply_sigmoid", button_pressed)


