extends Control

var weights
var sigmo = false
func _process(delta):

	for p in get_children():
		var ppos = p.get_pos()*Vector2(1,-1)/get_parent().rect_size
		
		var h1 = sigmoid(weights[0][0]*ppos.x + weights[0][3]*ppos.y + weights[1][2])
		var h2 = sigmoid(weights[0][1]*ppos.x + weights[0][4]*ppos.y + weights[1][5])
		var h3 = sigmoid(weights[0][2]*ppos.x + weights[0][5]*ppos.y + weights[1][8])
		
		
		var red_out = sigmoid(h1*weights[1][0] + h2*weights[1][3] + h3*weights[1][6] + weights[2][0])
		var blue_out = sigmoid(h1*weights[1][1] + h2*weights[1][4] + h3*weights[1][7] + weights[2][1])
		
		if(red_out>blue_out):
			p.blue()
		else:
			p.red()

func sigmoid(x):
	if sigmo:
		return 1/(1+exp(-x))
	else:
		return x

func _on_NN_Perceptron2_weight_changed(weights, sizes):
#	print(weights)
	self.weights = weights

func get_points_position_list():
	var pos = []
	for p in get_children():
		var ppos = p.get_pos()*Vector2(1,-1)/get_parent().rect_size
		pos += [ppos]
	return pos

func get_outputs():
	var out = []
	for p in get_children():
		out.append([0,1] if p.red else [1,0])
	return out


func _on_CheckButton_toggled(button_pressed):
	sigmo = button_pressed
