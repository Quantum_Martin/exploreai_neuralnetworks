extends Node2D


var t
var step = .5

var i = 0

var epoch = 0
var nepoch = 2500
var l_rate = 0.4

var x = [] # input
var y = [] # target output

var network = []

var L = 2

var w

func _ready():
	x = $Perceptron2/points.get_points_position_list()
	y = $Perceptron2/points.get_outputs()
	w = $Perceptron2/points.weights
	
#	print("Input : " + str(x))
#
#	var w_1 = [
#		{'weights':[w[0][0],w[0][3],w[1][2]]},
#		{'weights':[w[0][1],w[0][4],w[1][5]]},
#		{'weights':[w[0][2],w[0][5],w[1][8]]}
#		]
#	var w_2 = [
#		{'weights':[w[1][0],w[1][3],w[1][6],w[2][0]]},
#		{'weights':[w[1][1],w[1][4],w[1][7],w[2][1]]}
#		]
	randomize()
	var w_1 = [
		{'weights':[randf(),randf(),randf()]},
		{'weights':[randf(),randf(),randf()]},
		{'weights':[randf(),randf(),randf()]}
		]
	var w_2 = [
		{'weights':[randf(),randf(),randf(),randf()]},
		{'weights':[randf(),randf(),randf(),randf()]}
		]

	
	network.append(w_1)
	network.append(w_2)

func updt_w():
	w[0][0] = network[0][0]['weights'][0]
	w[0][1] = network[0][1]['weights'][0]
	w[0][2] = network[0][2]['weights'][0]
	w[0][3] = network[0][0]['weights'][1]
	w[0][4] = network[0][1]['weights'][1]
	w[0][5] = network[0][2]['weights'][1]
	w[1][0] = network[1][0]['weights'][0]
	w[1][1] = network[1][1]['weights'][0]
	w[1][2] = network[0][0]['weights'][2]
	w[1][3] = network[1][0]['weights'][1]
	w[1][4] = network[1][1]['weights'][1]
	w[1][5] = network[0][1]['weights'][2]
	w[1][6] = network[1][0]['weights'][2]
	w[1][7] = network[1][1]['weights'][2]
	w[1][8] = network[0][2]['weights'][2]
	
	w[2][0] = network[1][0]['weights'][3]
	w[2][1] = network[1][1]['weights'][3]
#	print(w)

var active = false

func _process(delta):
	if(Input.is_action_just_pressed("retro")):
		active = true
	if(active):
#		if(epoch<nepoch):
		print("epoch : " + str(epoch))
		epoch+=1
		train_network(network,x,y)
#		print(network)
		updt_w()
		$Perceptron2/NN_Perceptron2.set_weights_nn2(w)


func activate(weights, inputs):
	var a = weights[-1]
	for i in range(len(weights)-1):
		a += weights[i]*inputs[i]
	return a
func transfert(a):
	return 1.0/(1.0+exp(-a))

func d_transfert(output):
	return output*(1.0-output)

func forward_propagate(network, entry):
	var inputs = entry
	for layer in network:
		var new_inputs = []
		for neuron in layer:
			var a = activate(neuron['weights'], inputs)
			neuron['output'] = transfert(a)
			new_inputs.append(neuron['output'])
		inputs = new_inputs
	return inputs

func backpropagate(network, expected):
	for i in range(len(network)-1,-1,-1):
		var layer = network[i]
		var errors = []
		if i != len(network)-1:
			for j in range(len(layer)):
				var err = 0.0
				for neuron in network[i+1]:
					err+=(neuron['weights'][j] * neuron['delta'])
				errors.append(err)
		else:
			for j in range(len(layer)):
				var neuron = layer[j]
				errors.append(expected[j] - neuron['output'])
		for j in range(len(layer)):
			var neuron = layer[j]
			neuron['delta'] = errors[j]*d_transfert(neuron['output'])
		
func update_weights(network, row):
	for i in range(len(network)):
		var inputs = []
		if i != 0:
			for neuron in network[i-1]:
				inputs.append(neuron['output'])
		else:
			inputs = [row.x,row.y]
		for neuron in network[i]:
			for j in range(len(inputs)):
				neuron['weights'][j] += l_rate*neuron['delta']*inputs[j]
			neuron['weights'][-1] += l_rate * neuron['delta']

func train_network(network, x,y):
	var sum_error = 0
	for row in len(x):
		
		var outputs = forward_propagate(network, x[row])
#		print("entry: " + str(x[row]) + " expected : " + str(y[row]) + " | " + str([1,0] if outputs[0] > outputs[1] else [0,1]))
#		print(outputs)
		var expected = y[row]
		for i in range(len(expected)):
			sum_error += pow(expected[i]-outputs[i],2)
		backpropagate(network, expected)
		update_weights(network, x[row])
#	print("error" + str(sum_error))
	










