tool
extends Node2D

export(Array, int, 1,25) var layer_sizes = [] #setget set_layer_sizes

var synapse = preload("res://NeuralNetwork/Synapse/Synapse.tscn")

var layer_scene = preload("res://NeuralNetwork/Layer/Layer.tscn")

signal weight_changed(weights,sizes)

func _ready():
	set_layer_sizes(layer_sizes)
	update_weights()
	
	
func _process(delta):
	if(Input.is_action_just_pressed("ui_up")):
		layer_sizes[0]+=1
		set_layer_sizes(layer_sizes)
	if(Input.is_action_just_pressed("ui_down")):
		if(layer_sizes[0]>1):
			layer_sizes[0]-=1
		set_layer_sizes(layer_sizes)
	
#	var layer_count = get_child_count()
#
#	for l in range(layer_count):
#		if(l<layer_count-1): # Not for last layer
#			var layer = get_child(l)
#
#			var next_layer = get_child(l+1)
#
#			for neuron in layer.get_children():
#				for destination in next_layer.get_children():
#					var synapse_node = synapse.instance()
#					layer.add_child(synapse_node)
#					synapse_node.init_synapse(neuron.global_position, destination.global_position)

func set_layer_sizes(sizes):
	layer_sizes = sizes
	var layer_k = get_child_count()
	for layer_num in range(layer_k):
		if(layer_num>=len(sizes)):
			get_child(layer_num).queue_free()
	
	while(layer_k<len(sizes)):
		var layer_node = layer_scene.instance()
		add_child(layer_node)
		
		layer_node.position.x = 200*layer_k
		layer_k+=1
	
	for i in range(get_child_count()):
		get_child(i).layer_num = i
		get_child(i).update_neurons(sizes[i] if sizes[i]!=0 else 1)
	
	for i in range(get_child_count()-1):
		get_child(i).update_synapses(get_child(i+1), sizes[i+1] if sizes[i+1]!=0 else 1, self)
	
	get_child(get_child_count()-1).update_synapses(null,0,self)
	
func _unhandled_input(event):
	if(event is InputEventMouseButton and event.button_index == BUTTON_LEFT):
		for neuron in get_tree().get_nodes_in_group("Neurons"):
			neuron.hide_neuron_GUI()

func update_weights():
	var w = []
	var s = []
	for i in range(get_child_count()):
		w.append(get_child(i).get_weights())
		s.append(get_child(i).get_child_count())
		print("weights : " + str(w))
	emit_signal("weight_changed", w, s)

func set_weights_nn2(w):
	get_child(0).get_child(0).get_child(0).set_weight(w[0][0])
	get_child(0).get_child(0).get_child(1).set_weight(w[0][1])
	get_child(0).get_child(0).get_child(2).set_weight(w[0][2])
	get_child(0).get_child(1).get_child(0).set_weight(w[0][3])
	get_child(0).get_child(1).get_child(1).set_weight(w[0][4])
	get_child(0).get_child(1).get_child(2).set_weight(w[0][5])
	
	get_child(1).get_child(0).get_child(0).set_weight(w[1][0]) 
	get_child(1).get_child(0).get_child(1).set_weight(w[1][1]) 
	get_child(1).get_child(0).get_child(2).set_weight(w[1][2]) 
	get_child(1).get_child(1).get_child(0).set_weight(w[1][3]) 
	get_child(1).get_child(1).get_child(1).set_weight(w[1][4]) 
	get_child(1).get_child(1).get_child(2).set_weight(w[1][5])
	get_child(1).get_child(2).get_child(0).set_weight(w[1][6]) 
	get_child(1).get_child(2).get_child(1).set_weight(w[1][7]) 
	get_child(1).get_child(2).get_child(2).set_weight(w[1][8])
	
	get_child(2).get_child(0).get_child(0).set_weight(w[2][0]) 
	get_child(2).get_child(1).get_child(0).set_weight(w[2][1])
	update_weights()

	
