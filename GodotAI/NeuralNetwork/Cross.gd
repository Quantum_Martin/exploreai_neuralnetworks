extends ColorRect

export(bool) var red

func _ready():
	material.set_shader_param("color", Color.red if red else Color.blue)

func get_pos():
	return rect_position + 50/2*Vector2.ONE

func blue():
	material.set_shader_param("win", not red)
func red():
	material.set_shader_param("win", red)
